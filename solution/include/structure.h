/// @file
/// @brief File with structures

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define START_OFFSET  0x3c
#define MAGIC_SIZE  4
#define NAME_SIZE  8

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif 
/// @brief Struct of header for PEFile
struct
#ifdef __GNUC__
__attribute__((packed))
#endif 
PEHeader {
    uint8_t magic[MAGIC_SIZE];
    uint16_t Machine;
    uint16_t NumberOfSections;
    uint32_t TimeDateStamp;
    uint32_t PointerToSymbolTable;
    uint32_t NumberOfSymbols;
    uint16_t SizeOfOptionalHeader;
    uint16_t Characteristics;
};
/// @brief Struct of sections
struct
#ifdef __GNUC___
__attribute__((packed))
#endif
SectionHeader{
    char name[NAME_SIZE];
    uint32_t PhysicalAddress;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLineNumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberOfLineNumbers;
    uint32_t Characteristics;

};
/// @brief structure of PEFile
struct 
#ifdef _GNUC_
__attribute__((packed))
#endif
PEFile
{
  /// @name Offsets within file
  ///@{
  /// Offset to a main PE header
  uint32_t header_offset;
  /// Offset to a file magic
  uint32_t magic_offset;
  /// Offset to a section table
  uint32_t section_header_offset;
  ///@}
  
  /// @name File headers
  ///@{
  
  /// File magic
  uint32_t magic;
  /// Main header
  struct PEHeader header;
  /// Array of section headers with the size of header.number_of_sections
  struct SectionHeader *section_headers;
  ///@}
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

bool find_and_write(FILE* input, FILE* output, char* section);
