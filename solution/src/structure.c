/// @file
/// @brief File for working with PEFiles

#include "structure.h"

/// @brief Function that allocate you to PEHeader and read it
/// @param[in] f FILE* input - file that containts PEFile
/// @param[in] f struct PEFile* file - pointer to PEFile instance
/// @return success of operations
static bool set_headers(FILE* input, struct PEFile* file){
  fseek(input, START_OFFSET, SEEK_SET);
  fread(&(file->header_offset), 4, 1, input);
  fseek(input, file->header_offset, SEEK_SET);
  fread(&(file->header), sizeof(struct PEHeader), 1, input);
  fseek(input, file->header.SizeOfOptionalHeader, SEEK_CUR);
  return true;
}

/// @brief Function to allocate memory fo section headers
/// @param[in] f struct PEFile* file - pointer to PEFile instance
/// @param[in] f FILE* input - file that containts PEFile
/// @return Void, Sadness, Pain
static void initialize(struct PEFile* file, FILE* input){
    file->section_headers = malloc(sizeof(struct SectionHeader)*file->header.NumberOfSections);
    for(uint16_t i =0; i<file->header.NumberOfSections; i++){
        fread(file->section_headers+i, sizeof(struct SectionHeader), 1, input);
    }
}

/// @brief Function for searching section in header
/// @param[in] f FILE* input - file that containts PEFile
/// @param[in] f char* section - name of section that we are searching for
/// @return index of section r -1 if there is no one
static uint16_t found_section(struct PEFile* file, char* section){
  file->section_header_offset = file->header_offset+sizeof(struct PEHeader)+file->header.SizeOfOptionalHeader;
  for(uint16_t i = 0; i<file->header.NumberOfSections; i++){
    if(!strcmp((file->section_headers+i)->name, section)) return i;
  }
  return -1;
} 

/// @brief Function for searching searching from reading file up to getting section
/// @param[in] f FILE* input - file that containts PEFile
/// @param[in] f struct PEFile* file - pointer to PEFile instance
/// @param[in] f char* section - name of section that we are searching for
/// @return index of section r -1 if there is no one
static uint16_t get_section(FILE* input, struct PEFile* file, char* section){
  set_headers(input, file);
  
  initialize(file, input);

  uint16_t section_founded = found_section(file, section);

  return section_founded;
}

/// @brief Function for writting section to the file
/// @param[in] f struct SectionHeader* sh - pointer to the founded Section that we need to write to file
/// @param[in] f FILE* output - file for writting structure to
/// @param[in] f FILE* input - file that containts PEFile
/// @return success of function
static bool write(struct SectionHeader* sh, FILE* output, FILE* input){

  void* copied = malloc(sh->SizeOfRawData);

  fseek(input, sh->PointerToRawData, SEEK_SET);
  fread(copied, sh->SizeOfRawData, 1 ,input);
  fwrite(copied, sh->SizeOfRawData, 1, output);
  free(copied);
  return true;
}

/// @brief Function for compress all processes for searching section
/// @param[in] f FILE* input - file that containts PEFile
/// @param[in] f FILE* output - file for writting structure to
/// @param[in] f char* section - name of section that we are searching for
/// @return success of function
bool find_and_write(FILE* input, FILE* output, char* section){
  struct PEFile pefile = {0};
  uint16_t found_sectionn = get_section(input, &pefile, section);
  bool result = write((pefile.section_headers+found_sectionn), output, input);
  free(pefile.section_headers);
  return result;
}
