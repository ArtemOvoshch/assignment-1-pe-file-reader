/// @file 
/// @brief Main application file

#include "structure.h"
#include <stdio.h>
#include <stdlib.h>


/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning

  FILE* input = fopen(argv[1], "rb");
  if(!input){
    return -1;
  }

  FILE* output = fopen(argv[3], "wb");
  if(!output){
    fclose(input);
    return -1;
  }

  char* section_name = argv[2];

  find_and_write(input, output, section_name);

  fclose(input);
  fclose(output);

  return 0;
}
